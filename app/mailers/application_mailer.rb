class ApplicationMailer < ActionMailer::Base
  default from: 'contato@ensinohi.com.br'
  layout 'mailer'
end
