class User < ApplicationRecord
  has_many :messages
  EMAIL_REGEX = /^\S+@\S+$/
  NAME_REGEX = /\w+/
  validates :email , presence: true,
                     uniqueness:{case_sensetive:false},
                     format:{with:EMAIL_REGEX,multiline:true}
                       
  validates :username, presence: true, 
                       uniqueness: { case_sensitive: false }, 
                       format: { with: /\A#{NAME_REGEX}\z/i },
                       length: { maximum: 15 }
                      
  validates :password, presence: true, length: { minimum: 6 }
  has_secure_password
end
